package io.template;

import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sbt-velichko-aa on 30.12.2016.
 */
public class WorkWithExcelTest {

    private static Map<String, String> data;

    static {
        data = new HashMap<>();
        data.put("key1", "value1");
        data.put("key2", "value2");
        data.put("key3", "value3");
        data.put("key4", "value4");
        data.put("key5", "value5");
        data.put("key6", "value6");
        data.put("key7", "value7");
    }


    @Test
    public void testReadTheData() throws Exception {
        WorkWithExcel excel = new WorkWithExcel();

        excel.writeToExcel("src\\test\\resources\\testData\\result.xlsx", data);
//        System.out.println("work");
        HashMap res = WorkWithExcel.readTheData("src\\test\\resources\\testData\\data.xlsx", 0, "Test1");
//        System.out.println("work size: " + res.size());
//        Iterator it = res.keySet().iterator();
//        while (it.hasNext()) {
//            String key = it.next().toString();
//            System.out.println("key: " + key + ", value: " + res.get(key));
//        }
    }

}