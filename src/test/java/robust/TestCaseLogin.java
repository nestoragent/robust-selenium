package robust;

//TestNG

import io.template.Login;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

//Drivers and packages

/**
 * Created by nestor on 29.12.2016.
 */
public class TestCaseLogin {
    WebDriver driver;
    private String getTestCaseName;
    private int getTestCaseRow;

    @BeforeMethod
    public void VerifyLogin_Loginpage() {

        //Open browser
        driver = new FirefoxDriver();
        driver.manage().deleteAllCookies();
        Reporter.log("Browser is initiatized");
        driver.manage().window().maximize();
        Reporter.log("Browser is maximized");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test(dataProvider = "Excelinput")
    public void Authentication(String getUsername, String getPassword, String getltfNo) throws Exception {

        try {

            //Initiate URL
//            driver.get(dashboard_envVariables.URL);
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            Reporter.log("URL is initialized");

            //Login successfully
            Reporter.log("--Proceeding to Login page..");

            if (Login.login(driver, getUsername, getPassword) == "0")

            {
                Reporter.log("PASS - Credentials are added");
            }

            //  else if  (Login.errorcondition(driver).equals(2)) {

            //    return;

            //  }

            else

            {
                Reporter.log("FAIL - Credentials could not be passed");
            }

            Thread.sleep(2000);

            if (Login.errorcondition(driver) == "0")

            {
                Reporter.log("PASS - Login is successful");
            } else

            {
                Reporter.log("FAIL - Login was unsuccessful. Check error message");
            }

            Thread.sleep(2000);

        } catch (Exception e) {

            Reporter.log("Execution resulted in error");
            throw (e);
        }
    }


    @AfterMethod
    public void quitdriver() {

        //Close driver
        driver.quit();
    }


//    @DataProvider
//    public Object[][] Excelinput() throws Exception {
//
//         //Setting up the Test Data Excel file
//        commonUtils.setExcelFile("C://QA//login//src//test//java//datasheet//datasheet.xlsx", "Sheet1");
//        getTestCaseName = this.toString();
//        getTestCaseName = commonUtils.getTestCaseName(this.toString());
//        getTestCaseRow = commonUtils.getRowContains(getTestCaseName, 0);
//
//        //Creating object to return data in array
//        Object[][] testObjArray = commonUtils.getTableArray("C://QA//login//src//test//java//datasheet//datasheet.xlsx", "Sheet1", getTestCaseRow);
//        return (testObjArray);
//    }

}
