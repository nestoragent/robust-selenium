package io.template;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by nestor on 29.12.2016.
 */
public class WorkWithExcel {

    public static HashMap readTheData(String filePath, int sheetIndex, String testCaseName) {
        HashMap<String, String> result = new HashMap<>();
        FileInputStream fileInputStream;
        XSSFWorkbook workbook;
        File file = new File(filePath);
        try {
            if (file.exists()) {
                fileInputStream = new FileInputStream(file);
                workbook = new XSSFWorkbook(fileInputStream);
                workbook.setActiveSheet(sheetIndex);
                XSSFSheet sheet = workbook.getSheetAt(sheetIndex);
                Iterator<Row> rowIterator = sheet.rowIterator();

                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();
                    if (testCaseName.equalsIgnoreCase(row.getCell(0).toString()))
                        result.put(row.getCell(1).toString(), row.getCell(2).toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public void writeToExcel(String filePath, Map<String, String> data) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Active");

        try {
            int rowCount = 0;
            Set<String> keys = data.keySet();
            for (String key : keys) {
                Row row = sheet.createRow(rowCount++);

                Cell cellKey = row.createCell(0);
                cellKey.setCellValue(key);
                Cell cellValue = row.createCell(1);
                cellValue.setCellValue(data.get(key));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try (FileOutputStream outputStream = new FileOutputStream(filePath)) {
            workbook.write(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
