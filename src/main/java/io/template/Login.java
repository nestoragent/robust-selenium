package io.template;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import java.util.concurrent.TimeUnit;

/**
 * Created by nestor on 29.12.2016.
 */
public class Login {
    //declare vars being used in code
    static WebElement myvar;
    static WebElement myvar2;

    //Declare list of locators on a page
    static By username = By.id("username");
    static By password = By.xpath(".//*[@id='password']");
    static By clickbutton = By.xpath(".//*[@id='loginForm']/div[5]/div/button");
    static By forgotpassword = By.linkText("Forgot Password?");
    static By requestaccount = By.linkText("Request an Account");
    static By existingpagecontent = By.id("loginForm");
    static By nextpagecontent = By.className("dashboard-area");
    static By alertmessage = By.id("error-msg");

    //Begin forming classes
    public static String login(WebDriver driver, String uid, String pass) {


        myvar = (new WebDriverWait(driver, 130)).until(ExpectedConditions.presenceOfElementLocated(existingpagecontent));

        if (myvar.isDisplayed()) {

            driver.findElement(username).sendKeys(uid);
            driver.findElement(password).sendKeys(pass);
            driver.findElement(clickbutton).click();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            return "0";

        } else {
            return "1";
        }
    }

    //If it remains on same page, look for error message

    public static String errorcondition(WebDriver driver) {

        if (driver.findElement(nextpagecontent).isDisplayed()) {


            return "0";

        } else

        {
            myvar2 = driver.findElement(alertmessage);
            Reporter.log(myvar2.getText());
            return "1";
        }


    }
}
